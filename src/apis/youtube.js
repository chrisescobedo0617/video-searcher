import axios from "axios";

const KEY = "AIzaSyCcpI2L5x__3WV-aoXRO4LU7arB6U-Bbos";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    maxResults: 10,
    key: KEY,
  },
});
